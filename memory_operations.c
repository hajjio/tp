#include "memory_operations.h"
#include <stdlib.h>
#include <stdio.h>

void *my_memcpy(void *dst, const void *src, size_t len) {
    char* source = (char*) src;
    char* destination = (char*) dst;
    for(int i=0;i!=len;i++) {
        destination[i] = source[i];
    }
    return destination;
}

void *my_memmove(void *dst, const void *src, size_t len) {
    char* source = (char*) src;
    char* destination = (char*) dst;
    char* tmp = malloc(len*sizeof(char));
    for(int i=0;i!=len;i++) {
        tmp[i] = source[i];
    }
    for(int i=0;i!=len;i++) {
        destination[i] = tmp[i];
    }
    free(tmp);
    return destination;
}

int is_little_endian() {
    int a = 1;
    char *b = (char*)&a;
    return *b+48-'0';

}

int reverse_endianess(int value) {
    return ((value & 0xff) << 24) | ((value & 0xff00) << 8) | ((value & 0xff0000) >> 8) | ((value & 0xff000000) >> 24);
}
