#include "vecteur_dynamique.h"
#include <stdlib.h>

vecteur allouer_vecteur(int taille) {
    vecteur v = malloc(sizeof(vecteur));
    v->taille = taille;
    v->donnees = malloc(taille*sizeof(double));
    return v;
}

void liberer_vecteur(vecteur v) {
    free(v->donnees);
    free(v);
}

int est_vecteur_invalide(vecteur v) {
    return (v==NULL || v->donnees==NULL);
}

double *acces_vecteur(vecteur v, int i) {
    if (i<0) return NULL;
    else if (i<v->taille) {
        return (v->donnees+i);
    }
    else {
        void* res = realloc(v->donnees, (i+1)*sizeof(double));
        if (res == NULL) return NULL;
        v->taille = i+1;
        return (v->donnees+i);
    }
}

int taille_vecteur(vecteur v) {
    return v->taille;
}
